FROM ubuntu:18.04
RUN apt-get update \
    && apt-get install python3 python3-pip -y &&\
    pip install pipenv && \
    mkdir inmobilio && \
    cd inmobilio && \
    git clone https://gitlab.com/feliipepuentes/inmobilio_test.git . && \
    pipenv install && \
    pipenv shell && \
    python manage.py runserver

expose 8000