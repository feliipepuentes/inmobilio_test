# Generated by Django 2.1.3 on 2018-11-09 18:00

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Exterior',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vigilancia', models.CharField(max_length=500)),
                ('parqueadero', models.CharField(max_length=500)),
                ('salon_social', models.BooleanField()),
                ('numero_pisos', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='General',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('direccion', models.CharField(max_length=500)),
                ('ciudad', models.CharField(max_length=500)),
                ('departamento', models.CharField(max_length=500)),
                ('pais', models.CharField(max_length=500)),
                ('telefono', models.CharField(max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='Interior',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cuartos', models.FloatField()),
                ('banos', models.IntegerField()),
                ('closets', models.IntegerField()),
                ('calentador', models.BooleanField()),
            ],
        ),
    ]
