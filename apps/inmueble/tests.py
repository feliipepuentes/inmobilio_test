from django.test import TestCase
from .serializers import InmuebleSerializer
from .models import Inmueble

data = {
    'tipo': 'casa',
    'subtipo': 'rural',
    'general': {
        'direccion': 'Casa 1 # 1',
        'ciudad': 'Floridablanca',
        'departamento': 'Santander',
        'pais': 'Colombia',
        'telefono': '1234'
    },
    'interior': {
        'cuartos': 3,
        'banos': 2,
        'closets': 3,
        'calentador': True
    },
    'exterior': {
        'vigilancia': 'Dos celadores',
        'parqueadero': 'Dos plantas',
        'salon_social': False,
        'numero_pisos': 3
    }
}

data_edited = {
    'tipo': 'apartamento',
    'subtipo': 'rural',
    'general': {
        'direccion': 'Casa 1 # 1',
        'ciudad': 'Floridablanca',
        'departamento': 'Santander',
        'pais': 'Colombia',
        'telefono': '1234'
    },
    'interior': {
        'cuartos': 3,
        'banos': 2,
        'closets': 3,
        'calentador': True
    },
    'exterior': {
        'vigilancia': 'Dos celadores',
        'parqueadero': 'Dos plantas',
        'salon_social': False,
        'numero_pisos': 3
    }
}

class InmuebleTest(TestCase):
    def setUp(self):
        query = InmuebleSerializer(data=data)
        if query.is_valid():
            query.save()
        
    def test_if_update(self):
        serializer = InmuebleSerializer(query, data_edited)
        self.assertEqual(serializer.data['tipo'], 'apartamento')

    

