from django.db import models

# Create your models here.

"""
Definición de los modelos, cuando blank es False significa que ese campo es requerido
"""

class General(models.Model):
    direccion       = models.CharField(max_length=500, blank = False)
    ciudad          = models.CharField(max_length=500, blank = False)
    departamento    = models.CharField(max_length=500, blank = False)
    pais            = models.CharField(max_length=500, blank = False)
    telefono        = models.CharField(max_length=500, blank = False)


class Interior(models.Model):
    cuartos         = models.FloatField(blank = False)
    banos           = models.IntegerField(blank = False)
    closets         = models.IntegerField(blank = False)
    calentador      = models.BooleanField(blank = True)

class Exterior(models.Model):
    vigilancia      = models.CharField(max_length=500, blank = False)
    parqueadero     = models.CharField(max_length=500, blank = False)
    salon_social    = models.BooleanField(blank = True)
    numero_pisos    = models.IntegerField(blank = False)


class Inmueble(models.Model):
    tipo            = models.CharField(max_length=500, blank = False)
    subtipo         = models.CharField(max_length=500, blank = False)
    general         = models.ForeignKey(General, on_delete= models.DO_NOTHING, blank = False)
    interior        = models.ForeignKey(Interior, on_delete= models.DO_NOTHING, blank = False)
    exterior        = models.ForeignKey(Exterior, on_delete= models.DO_NOTHING, blank = False)