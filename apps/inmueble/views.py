import json

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework import serializers
from rest_framework.decorators import api_view
from rest_framework.renderers import JSONRenderer
from .models import Inmueble
from apps.inmueble.models import Interior

from .serializers import InmuebleSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

class crear_inmuebles(APIView):

	@csrf_exempt
	def post(self, request):
		"""
	Creación de nuevo inmueble

    ---
    parametros:
    {
		'tipo': string,
		'subtipo': string,
		'general':
		{
			'direccion'		: string,
			'ciudad'		: string,
			'departamento'	: string,
			'pais'			: string,
			'telefono'		: string
		},
		'interior':
		{
			'cuartos'	: float,
			'banos'  	: integer,
			'closets'	: integer,
			'calentador': boolean  
		},
		'exterior':
		{
			'vigilancia'	: string,
			'parquadero'	: string,
			'salon_social'	: boolean,
			'numero_pisos'	: integer
		}
	}
		"""
		
		data = request.data
		data_serialized = InmuebleSerializer(data=data)
		if data_serialized.is_valid():
			data_serialized.save()
			#Objecto de respuesta luego de la creación del nuevo inmueble
			return Response({'msg': 'Inmueble creado', 'inmueble': json.loads(JSONRenderer().render(data_serialized.data))})
		else:
			return Response(data_serialized.errors, status = 400)


class listar_inmuebles(APIView):

	@csrf_exempt
	def get(self, request):
		"""
		Listar inmuebles existentes
		"""
		queryset = Inmueble.objects.all()
		data_ready = []
		for item in queryset:
			data_serialized = InmuebleSerializer(item)
			data_ready.append(json.loads(JSONRenderer().render(data_serialized.data)))
		return Response(data_ready)


class actualizar_inmueble(APIView):

	@csrf_exempt
	def get(self,request,id):
		"""
		Listar inmueble por id
		"""

		try:
			queryset = Inmueble.objects.get(pk=id)
		except:
			return Response('Este inmueble no existe')	
		else:
			data_ready = []
			data_serialized = InmuebleSerializer(queryset)
			data_ready.append(json.loads(JSONRenderer().render(data_serialized.data)))
			return Response({'inmueble':data_ready})

	def put(self,request,id):	
		"""
	Edición de inmueble

    ---
    parametros:
    {
		'tipo': string,
		'subtipo': string,
		'general':
		{
			'direccion'		: string,
			'ciudad'		: string,
			'departamento'	: string,
			'pais'			: string,
			'telefono'		: string
		},
		'interior':
		{
			'cuartos'	: float,
			'banos'  	: integer,
			'closets'	: integer,
			'calentador': boolean  
		},
		'exterior':
		{
			'vigilancia'	: string,
			'parquadero'	: string,
			'salon_social'	: boolean,
			'numero_pisos'	: integer
		}
	}
		"""
		data 	= request.data
		model 	= Inmueble.objects.get(pk=id)
		data_serialized = InmuebleSerializer(model, data=data)
		if data_serialized.is_valid():
			data_serialized.save()
			return Response({'msg': 'Inmueble editado', 'inmueble': json.loads(JSONRenderer().render(data_serialized.data))})
		else:
			return Response(data_serialized.errors, status = 400)
	
	def delete(self,request,id):
		"""
		Eliminar inmueble
		"""
		try:
			model 	= Inmueble.objects.get(pk=id)
		except:
			return Response({'ERR': 'El inmueble no existe'}, status=400)
		else:
			model.general.delete()
			model.interior.delete()
			model.exterior.delete()
			model.delete()
			return Response({'msg': 'Inmueble eliminado'})