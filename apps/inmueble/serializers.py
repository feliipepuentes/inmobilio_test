from rest_framework import routers, serializers, viewsets
from apps.inmueble.models import Inmueble,General,Interior,Exterior

class GeneralSerializer(serializers.ModelSerializer):
	class Meta:
		model = General
		# fields = '__all__'
		fields = ('direccion', 'ciudad', 'departamento', 'pais', 'telefono')

class InteriorSerializer(serializers.ModelSerializer):
	class Meta:
		model = Interior
		# fields = '__all__'
		fields = ('cuartos', 'banos', 'closets', 'calentador')

class ExteriorSerializer(serializers.ModelSerializer):
	class Meta:
		model = Exterior
		# fields = '__all__'
		fields = ('vigilancia', 'parqueadero', 'salon_social', 'numero_pisos')

class InmuebleSerializer(serializers.ModelSerializer):
	general 		= GeneralSerializer()
	interior 		= InteriorSerializer()
	exterior 		= ExteriorSerializer()

	class Meta:
		model 	= Inmueble
		# fields = '__all__'
		fields 	= ('id', 'tipo', 'subtipo', 'general', 'interior', 'exterior') 

	def create(self, validated_data):
		"""
		Creación de nuevo inmueble, este usa los serializadores de cada modelo
		"""
		general_data 	= validated_data.pop('general')
		interior_data 	= validated_data.pop('interior')
		exterior_data 	= validated_data.pop('exterior')
		
		general_model 	= General.objects.create(**general_data)
		interior_model	= Interior.objects.create(**interior_data)
		exterior_model	= Exterior.objects.create(**exterior_data)

		Inmueble_model = Inmueble.objects.create(general = general_model, interior = interior_model, exterior = exterior_model, **validated_data)

		return Inmueble_model

	def update(self, instance, validated_data):
		"""
		Actualización de nuevo inmueble, este usa los serializadores de cada modelo
		"""
		general_data 	= validated_data.pop('general')
		interior_data 	= validated_data.pop('interior')
		exterior_data 	= validated_data.pop('exterior')

		instance.__dict__.update(**validated_data)
		instance.general.__dict__.update(**general_data)
		instance.interior.__dict__.update(**interior_data)
		instance.exterior.__dict__.update(**exterior_data)
		instance.save()
		instance.general.save()
		instance.interior.save()
		instance.exterior.save()
		return instance