# Inmobilio backend development test

En este proyecto se soluciona la prueba enviada, que se puede encontrar en [este link](https://gitlab.com/inmobilio-interview/backend-python)

## Endpoints

El proyecto cuenta con los siguientes endpoints implementados

 - /inmuebles [GET] listar los inmuebles existentes
 - /inmueble  [POST] crear un nuevo inmueble
 - /inmueble/{id} [GET] listar un inmueble especifico
 - /inmueble/{id} [PUT] editar un inmueble creado
 - /inmueble/{id} [DELETE] eliminar un inmueble existente
 
## Objeto JSON para crear inmuebles 

    {
		'tipo': string,
		'subtipo': string,
		'general':
			{
			'direccion' : string,
			'ciudad' : string,
			'departamento' : string,
			'pais' : string,
			'telefono' : string
			},	
		'interior':
			{
			'cuartos' : float,
			'banos' : integer,
			'closets' : integer,
			'calentador': boolean
			},
		'exterior':
			{
			'vigilancia' : string,
			'parquadero' : string,
			'salon_social' : boolean,
			'numero_pisos' : integer
			}
	}


