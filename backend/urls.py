"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
import apps
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Inmobilio API')

"""
Se definen las urls que van a enrutar el proyecto

* api/doc define la ruta de documentación
* inmueble define la ruta para administrar los inmuebles
"""

urlpatterns = [
    path('api/doc', schema_view),
    path('inmueble', include('apps.inmueble.urls')),
]

